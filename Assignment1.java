package selenium_l1;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.openqa.selenium.By;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Assign_01 {

	public static void main(String[] args)throws IOException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver","C:\\Users\\RA20066292\\Downloads\\chromedriver_win32\\chromedriver.exe");
		WebDriver d1=new ChromeDriver();
		//home page 
		d1.get("https://demo.opencart.com");
		d1.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		d1.findElement(By.xpath("//a[contains(@href,'https://demo.opencart.com/index.php?route=account/account')]")).click();
		d1.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		//login
		d1.findElement(By.xpath("//a[contains(text(),'Login')]")).click();
		d1.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		//extracting email and password from xls file
		FileInputStream f1=new FileInputStream("C:\\Users\\RA20066292\\Downloads\\login.xls");
		HSSFWorkbook workbook = new HSSFWorkbook(f1);
		HSSFSheet sheet = workbook.getSheet("Sheet1");
		String email =sheet.getRow(0).getCell((short)0).toString();
		String password =sheet.getRow(0).getCell((short)1).toString();
		
		d1.findElement(By.xpath("//input[@id='input-email']")).sendKeys(email);
		d1.findElement(By.xpath("//input[@id='input-password']")).sendKeys(password);
		d1.findElement(By.xpath("//input[@class='btn btn-primary']")).click();
		d1.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		//logout
		d1.findElement(By.xpath("//a[contains(@href,'https://demo.opencart.com/index.php?route=account/account')]")).click();
		d1.findElement(By.xpath("//ul[@class='dropdown-menu dropdown-menu-right']/li[5]/a[@href='https://demo.opencart.com/index.php?route=account/logout']")).click();
		d1.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		d1.close();


	}

}

